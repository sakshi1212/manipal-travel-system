﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Vehicle_Page : System.Web.UI.Page
{
    public static int counter=0;
    string seldate = "";

    //string username;
    //string userid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(counter==0)
        {
        //vehicletypelist.Items.Clear();
            vehicletypelist.Items.Add("Car");
        vehicletypelist.Items.Add("Bike");
        vehicletypelist.Items.Add("Cycle");
        counter++;
    }
        vehiclemodellist.Items.Clear();
        if (vehicletypelist.SelectedIndex == 0)
        {
            vehiclemodellist.Items.Add("santro");
            vehiclemodellist.Items.Add("innova");
        }
        else if (vehicletypelist.SelectedIndex == 1)
        {
            vehiclemodellist.Items.Add("activa");
            vehiclemodellist.Items.Add("pulsor");
        }
        else
        {
            vehiclemodellist.Items.Add("herohonda");
            vehiclemodellist.Items.Add("hercules");
        }

    }
    //protected void Update()
    //{
    //    detailslabel.Text = "hurray";
    //}
    protected void vehiclecalendar_SelectionChanged(object sender, EventArgs e)
    {
        //.Text = "You selected these dates:<br />";

        DateTime dt= vehiclecalender.SelectedDate;
        //{
        seldate = dt.ToLongDateString();
        date.Text += dt.ToLongDateString() ;
       // }

    }
    protected void VehicleWizard_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        string vehtype = Convert.ToString(vehicletypelist.SelectedItem);
        string vehmodel = Convert.ToString(vehiclemodellist.SelectedItem);
        int driven=0;
        if (opt1.Checked == true)
            driven++;
        int fullday = 0;
        if (opta.Checked == true)
            fullday++;
       string username = Request.QueryString["username"];
       string userid = Request.QueryString["userid"];

        //detailslabel.Text = username + userid;
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = @"Data Source=(localdb)\v11.0;Initial Catalog=manipaltravel;Integrated Security=True;Pooling=False";
        string adduser = "INSERT INTO [Vehbook] (id,un,vehtype,vehmod,driven,fullday,date) VALUES ('" + userid + "','";
        adduser += username + "','";
        adduser += vehtype + "','";
        adduser += vehmodel + "','";
        adduser += driven + "','";
        adduser += fullday + "','";
        adduser += date.Text + "')";
        SqlCommand cmd = new SqlCommand(adduser, conn);
        int added = 0;
       // string brn;
        try
        {
            conn.Open();
            added = cmd.ExecuteNonQuery();
            detailslabel.Text = " Your booking is confirmed. The details are: VEHICLE TYPE:" + vehtype + "    VEHICLE MODEL:" + vehmodel + "    DRIVEN: " + driven + "    DURATION:" + fullday + "    DATE :" + date.Text; 
            //int maxbrn;
            //string searchid = "SELECT MAX(bookrefnum)as maxbrn FROM [Vehbook]";

            //SqlCommand cmd2 = new SqlCommand(searchid, conn);
            //SqlDataReader reader;
            ////    reader = cmd2.ExecuteReader();
            //    reader.Read();

            //   brn = Convert.ToString(reader["maxbrn"]);

             //reader.Close();
        }
        catch (Exception err)
        {
            detailslabel.Text = "error" + err.Message+adduser;
        }
        finally { conn.Close(); }
    }

    protected void logout_Click(object sender, EventArgs e)
    {
        Response.Redirect("UserPage.aspx");
    }
  
    
}