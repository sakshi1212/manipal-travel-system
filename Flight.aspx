﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Flight.aspx.cs" Inherits="Flight" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <br< />
    <asp:Wizard ID="FlightWizard" runat="server" ActiveStepIndex="0" BackColor="LemonChiffon" BorderStyle="Groove" BorderWidth="2px" CellPadding="10" Width="788px" OnFinishButtonClick="FlightWizard_FinishButtonClick">
        <WizardSteps>
                <asp:WizardStep ID="dates" runat="server" Title="Step 1 - Choose Departure Date">
                Choose a Departure Date:<br  />
                 <asp:Calendar ID="Flightcalendar" runat="server"  Width="194px" OnSelectionChanged="Flightcalendar_SelectionChanged">
                </asp:Calendar><br  />
                    The Date Chosen is : <asp:Label ID="date" runat="server" Text="--" /><br /><br />
                 </asp:WizardStep>
                
            <asp:WizardStep ID="sd" runat="server" Title="Step 2 - Choose Destination">
                     Departure : MANGALORE <br  />
              <br />
                Choose a destination option :<br  />
              <br />
                 <asp:DropDownList ID="destinationlist" runat="server" Width="194px">
                    </asp:DropDownList><br  />
                </asp:WizardStep>
               
            <asp:WizardStep ID="WizardStep1" runat="server" Title="Step 3 - Choose Airline">
                    
                Choose an Airline :<br  />
              <br />
                 <asp:DropDownList ID="airline" runat="server" Width="194px">
                    </asp:DropDownList><br  />
                </asp:WizardStep>
            
            <asp:WizardStep ID="details" runat="server" StepType="Complete" Title="Details of Booking">  
                    <asp:Label ID="detailslabel" runat="server" Text="-"></asp:Label>
                     </asp:WizardStep>
           
             </WizardSteps>
        </asp:Wizard>
    </p><p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <asp:Button id="logout" runat="server" Text="LOGOUT" OnClick="logout_Click"/>
    </p>
</asp:Content>

