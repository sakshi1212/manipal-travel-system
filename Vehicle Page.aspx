﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Vehicle Page.aspx.cs" Inherits="Vehicle_Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <br< />
    <asp:Wizard ID="VehicleWizard" runat="server" ActiveStepIndex="0" BackColor="LemonChiffon" BorderStyle="Groove" BorderWidth="2px" CellPadding="10" Width="788px" OnFinishButtonClick="VehicleWizard_FinishButtonClick">
        <WizardSteps>
                <asp:WizardStep ID="type" runat="server" Title="Step 1 - Choose Vehicle Type">
                Choose a Vehicle Type:<br  />
                <asp:DropDownList ID="vehicletypelist" runat="server" autopostback="true" Width="194px">
                    </asp:DropDownList><br  />
                <br  />
                Choose a vehicle model:<br  />
                <asp:DropDownList ID="vehiclemodellist" runat="server" Width="194px">
                </asp:DropDownList>
                 </asp:WizardStep>
                
            <asp:WizardStep ID="driver" runat="server" Title="Step 2 - Choose Driver Option">
                    Choose a driver option :<br  />
              <br />
                  <asp:RadioButton id ="opt1" runat="server" GroupName="driverradiobutton" Text="Driven by Driver" />
                   <br /> 
                <asp:RadioButton id ="opt2" runat="server" GroupName="driverradiobutton" Text="Self-Driven" />
                 <br  />
                </asp:WizardStep>
               
             <asp:WizardStep ID="duration" runat="server" Title="Step 3- Choose Date and Duration">
                Choose a date :<br  />
                <asp:Calendar ID="vehiclecalender" runat="server"  Width="194px" OnSelectionChanged="vehiclecalendar_SelectionChanged">
                </asp:Calendar><br  />
                 The Date Chosen is : <asp:Label ID="date" runat="server" Text="--" />
                <br  />
                 <br />
                 <br />
                 Choose Duration :<br />
                <asp:RadioButton ID="opta" runat="server" GroupName="durationgrp" Text="Full Day" /><br />
                 <asp:RadioButton ID="optb" runat="server" GroupName="durationgrp" Text="Half Day" />
                 <br />
                </asp:WizardStep>
                <asp:WizardStep ID="details" runat="server" StepType="Complete" Title="Details of Booking">  
                    <asp:Label ID="detailslabel" runat="server" Text="-"></asp:Label>
                     <br />
                    <br />
                    <br />
                     </asp:WizardStep>
           
             </WizardSteps>
        </asp:Wizard>
    </p>
    <p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <asp:Button id="logout" runat="server" Text="LOGOUT" OnClick="logout_Click"/>
    </p>
    </asp:Content>

