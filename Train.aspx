﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Train.aspx.cs" Inherits="Train" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <p>
        <br< />
    <asp:Wizard ID="TrainWizard" runat="server" ActiveStepIndex="0" BackColor="LemonChiffon" BorderStyle="Groove" BorderWidth="2px" CellPadding="10" Width="788px" OnFinishButtonClick="TrainWizard_FinishButtonClick">
        <WizardSteps>
                <asp:WizardStep ID="type" runat="server" Title="Step 1 -Choose Destination">
                  Departure from UDUPI<br  />
                Choose Destination:<br  />
                <asp:DropDownList ID="traindestination" runat="server"  Width="194px">
                    </asp:DropDownList><br  />
                <br  />
               
                 </asp:WizardStep>
                
           
               
             <asp:WizardStep ID="duration" runat="server" Title="Step 2- Choose Date">
                Choose a date :<br  />
                <asp:Calendar ID="traincalendar" runat="server"  Width="194px" OnSelectionChanged="traincalendar_SelectionChanged">
                </asp:Calendar><br  />
                 The Date Chosen is : <asp:Label ID="date" runat="server" Text="--" />
                <br  />
                 <br />
                 <br />
                 
                </asp:WizardStep>
                <asp:WizardStep ID="details" runat="server" StepType="Complete" Title="Details of Booking">  
                    <asp:Label ID="detailslabel" runat="server" Text="-"></asp:Label>
                     </asp:WizardStep>
           
             </WizardSteps>
        </asp:Wizard>
    </p><p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <asp:Button id="logout" runat="server" Text="LOGOUT" OnClick="logout_Click"/>
    </p>
</asp:Content>

